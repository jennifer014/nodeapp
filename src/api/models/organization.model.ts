
var Sequelize_org = require('Sequelize');
const sequelize_org = require('../../configs/db.config');

var Organization_model = sequelize_org.define('organizations', {
    id_organization: {
      type: Sequelize_org.INTEGER,
      field: 'id_organization',
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize_org.STRING,
      field: 'name'
    },
    status: {
      type: Sequelize_org.INTEGER,
      field: 'status'
    }
  }, {
    timestamps: false
  });
  
module.exports = Organization_model;


const Sequelize_metric = require('Sequelize');
const sequelize_metric = require('../../configs/db.config');
const Repository_metric = require('../models/repositorry.model');

const Metric_model = sequelize_metric.define('metrics', {
  id_metric: {
      type: Sequelize_metric.INTEGER,
      field: 'id_metric',
      primaryKey: true,
      autoIncrement: true,
    },
    coverage: {
      type: Sequelize_metric.INTEGER,
      field: 'coverage'
    },
    bugs: {
      type: Sequelize_metric.INTEGER,
      field: 'bugs'
    },
    vulnerabilities: {
      type: Sequelize_metric.INTEGER,
      field: 'vulnerabilities'
    },
    hotspost: {
      type: Sequelize_metric.INTEGER,
      field: 'hotspost'
    },
    codesmells: {
      type: Sequelize_metric.INTEGER,
      field: 'codesmells'
    },
    id_repository: {
      type: Sequelize_metric.INTEGER,
      field: 'id_repository',
    },
  }, {
    timestamps: false
  });
  Metric_model.belongsTo(Repository_metric, {
    foreignKey: {
      name: 'id_repository',
    }
  });

module.exports = Metric_model;

module.exports = Object.freeze({
    state: {
        E: 'Enable',
        D: 'Disable',
        A: 'Archived'
    },
    status: {
        A: 'Active',
        I: 'Inactive'
    },
    code_verificacion: {
        604: 'Verificado',
        605: 'En espera',
        606: 'Aprobado'
    }
});

const Metric_ = require('../models/metric.model');
const Tribu_ = require('../models/tribu.model');
const Repository_ = require('../models/repositorry.model');
const constants = require('../constants/constants');

var request = require('request');
const https = require('http')
const { Op } = require("sequelize");


const CsvParser = require("json2csv").Parser;
const Organization_ = require('../models/organization.model');

const yearNow = ['2022-01-01', '2022-12-31'];


async function getMetrics(request, response, next) {
  try {
    getDataMetrics(request, response, false);
  } catch (err) {
    console.error(`Error while getting programming languages`, err.message);
    next(err);
  }
}

async function downloadCsv(request, response, next) {
  try {
    getDataMetrics(request, response, true);
  } catch (err) {
    console.error(`Error while getting programming languages`, err.message);
    next(err);
  }
}

async function getDataMetrics(request, response, download) {
  let respositories = [];
  let promise = new Promise((resolve, reject) => {
    var data = '';
    https.get('http://localhost:8081/', { json: true }, res => {
      res.on('data', chunk => { data += chunk })
      res.on('end', () => {
        resolve(JSON.parse(data));
      })
    });
  });
  let result = await promise; // wait until the promise resolves
  const idTribu = parseInt(request.params.idTribu);
  let tribuFind = null;
  await Tribu_.findOne({ raw: true, where: { id_tribe: idTribu } }).then(function (dataTribu) {
    tribuFind = dataTribu;
  });
  let infoMetrics = null;
  if (tribuFind) {
    respositories = result['repositories'];
    await Metric_.findAll({
      raw: true, where: { coverage: { [Op.gte]: 75 } }, include: [{
        model: Repository_,
        where: {
          id_tribe: tribuFind.id_tribe,
          state: 'E', 
	  create_time: {[Op.between]: yearNow}
        },
        attributes: ['name', 'state'],
        include: {
          model: Tribu_,
          attributes: ['name'],
          include: {
            model: Organization_,
            attributes: ['name'],
          }
        }
      }
      ]
    }).then(function (result) {
      infoMetrics = result;
    });

    if (infoMetrics && infoMetrics.length > 0) {
      let data = [];
      infoMetrics.forEach(element => {
  
        const resultado = respositories.find(repo => Number(repo.id) === Number(element.id_repository));
        element.verificacionState = constants.code_verificacion[resultado.state];
        const { coverage, codesmells, bugs, vulnerabilities, hotspost } = element;
        const dataMetric = { coverage, codesmells, bugs, vulnerabilities, hotspost };
  
        dataMetric['id'] = element.id_repository;
        dataMetric['name'] = element['repository.name'];
        dataMetric['state'] = constants.state[element['repository.state']];
        dataMetric['tribe'] = element['repository.tribu.name'];
        dataMetric['organization'] = element['repository.tribu.organization.name'];
        dataMetric['verificacionState'] =element.verificacionState;
        data.push(dataMetric);
      });
      if (download) {
        const csvFields = ["coverage", "codesmells", "bugs", "vulnerabilities",
          "id:", "name", "state", "tribe", "organization"];
        const csvParser = new CsvParser({ csvFields });
        const csvData = csvParser.parse(data);
        response.setHeader("Content-Type", "text/csv");
        response.setHeader("Content-Disposition", "attachment; filename=metricas.csv");
        response.status(200).end(csvData);
      } else {
        response.status(200).send({ message: 'Objects find', 'repositories': data })
      }
    } else {
      response.status(200).send({ message: 'La Tribu no tiene repositorios que cumplan con la cobertura necesaria' });
    }
  } else {
    response.status(200).send({ message: 'La Tribu no se encuentra registrada' });
  }
  
}
module.exports = {
  getMetrics,
  downloadCsv
};

const Organization = require('../models/organization.model');
async function getOrganizations(request, response, next) {
  try {
    await Organization.findAll({ raw: true }).then(function (personas) {
      response.status(200).send({ message: 'Objects find', 'data': personas })
    });
  } catch (err) {
    console.error(`Error while getting programming languages`, err.message);
    next(err);
  }
}

async function getOrganization(request, response, next) {
  try {
    const id = parseInt(request.params.id)
    await Organization.findAll({ raw: true, where: { id_organization: id } }).then(function (personas) {
      response.status(200).send({ message: 'Objects find', 'data': personas })
    });
  } catch (err) {
    console.error(`Error while getting programming languages`, err.message);
    next(err);
  }

}

async function saveOrganization(request, response, next) {
  try {
    const obj = await Organization.create(request.body);
    response.status(200).send({ message: 'Object created', 'data': obj.dataValues })
  } catch (err) {
    console.error(`Error while getting programming languages`, err.message);
    next(err);
  }
}

async function updateOrganization(request, response, next) {
  try {
    const id = parseInt(request.params.id);
    const obj = await Organization.update(request.body, {
      where: {
        id_organization: id,
      }
    });

    if (obj && obj.length > 0 && obj[0] === 1) {
      response.status(200).send({ message: 'Object modified', 'data': request.body })
    } else {
      response.status(200).send({ message: 'Object not modified', 'data': null })
    }
  } catch (err) {
    console.error(`Error while getting programming languages`, err.message);
    next(err);
  }

}
async function deleteOrganization(request, response, next) {
  try {
    const id = parseInt(request.params.id)
    const obj = await Organization.destroy({
      where: {
        id_organization: id,
      }
    });
    if (obj && obj === 1) {
      response.status(200).send({ message: 'Object deleted', 'data': request.body })
    } else {
      response.status(200).send({ message: 'Object not deleted', 'data': null })
    }
  } catch (err) {
    console.error(`Error while getting programming languages`, err.message);
    next(err);
  }
}


module.exports = {
  getOrganizations,
  getOrganization,
  saveOrganization,
  updateOrganization,
  deleteOrganization
};
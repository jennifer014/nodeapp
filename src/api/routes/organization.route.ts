const express_org = require('express');
const router_org = express_org.Router();

const organizationController = require('../controllers/organizations.controller');

router_org.get('/', organizationController.getOrganizations)
router_org.get('/:id', organizationController.getOrganization)
router_org.post('/', organizationController.saveOrganization)
router_org.put('/:id', organizationController.updateOrganization)
router_org.delete('/:id', organizationController.deleteOrganization)

module.exports = router_org;

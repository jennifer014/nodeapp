const express_metric = require('express');
const router_metric = express_metric.Router();

const metricController = require('../controllers/metric.controller');

router_metric.get('/:idTribu', metricController.getMetrics)
router_metric.get('/download/:idTribu', metricController.downloadCsv)

module.exports = router_metric;

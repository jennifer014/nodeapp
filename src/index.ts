const express = require("express")
var app = express()
const hostname = '192.168.1.162';
const bodyParser = require('body-parser')
const organizationsRoutes = require('./api/routes/organization.route');
const metricRoutes = require('./api/routes/metric.route');

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.use(bodyParser.json())


app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})


app.use('/organizations', organizationsRoutes);
app.use('/metrics', metricRoutes);

app.listen(10000, function () {
console.log("Started application on port %d", 10000)
});

module.exports = app;

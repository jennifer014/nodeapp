var express = require("express");
var app = express();
var hostname = '192.168.1.162';
var bodyParser = require('body-parser');
var organizationsRoutes = require('./api/routes/organization.route');
var metricRoutes = require('./api/routes/metric.route');
app.use(bodyParser.urlencoded({
    extended: true,
}));
app.use(bodyParser.json());
app.get('/', function (request, response) {
    response.json({ info: 'Node.js, Express, and Postgres API' });
});
app.use('/organizations', organizationsRoutes);
app.use('/metrics', metricRoutes);
app.listen(10000, function () {
    console.log("Started application on port %d", 10000);
});
module.exports = app;

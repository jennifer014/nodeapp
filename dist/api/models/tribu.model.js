var Sequelize_tribu = require('Sequelize');
var sequelize_tribu = require('../../configs/db.config');
var Organization_tribu = require('../models/organization.model');
var Tribu_model = sequelize_tribu.define('tribu', {
    id_tribe: {
        type: Sequelize_tribu.INTEGER,
        field: 'id_tribe',
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: Sequelize_tribu.STRING,
        field: 'name'
    },
    status: {
        type: Sequelize_tribu.INTEGER,
        field: 'status'
    },
    id_organization: {
        type: Sequelize_tribu.INTEGER,
        field: 'id_organization',
    },
}, {
    timestamps: false
});
Tribu_model.belongsTo(Organization_tribu, {
    foreignKey: {
        name: 'id_organization'
    }
});
module.exports = Tribu_model;

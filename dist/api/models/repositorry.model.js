var Sequelize_repo = require('Sequelize');
var sequelize_repo = require('../../configs/db.config');
var Tribu_repo = require('../models/tribu.model');
var Repository_model = sequelize_repo.define('repositorys', {
    id_repository: {
        type: Sequelize_repo.INTEGER,
        field: 'id_repository',
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: Sequelize_repo.STRING,
        field: 'name'
    },
    status: {
        type: Sequelize_repo.STRING,
        field: 'status'
    },
    state: {
        type: Sequelize_repo.STRING,
        field: 'state'
    },
    create_time: {
        type: Sequelize_repo.DATE,
        field: 'create_time'
    },
    year_create: {
        type: Sequelize_repo.INTEGER,
        field: 'year_create'
    },
    id_tribe: {
        type: Sequelize_repo.STRING,
        field: 'id_tribe',
    },
}, {
    timestamps: false
});
Repository_model.belongsTo(Tribu_repo, {
    foreignKey: {
        name: 'id_tribe'
    }
});
module.exports = Repository_model;

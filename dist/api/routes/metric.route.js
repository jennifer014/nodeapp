var express_metric = require('express');
var router_metric = express_metric.Router();
var metricController = require('../controllers/metric.controller');
router_metric.get('/:idTribu', metricController.getMetrics);
router_metric.get('/download/:idTribu', metricController.downloadCsv);
module.exports = router_metric;
